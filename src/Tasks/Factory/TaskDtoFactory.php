<?php
namespace Jeancsil\Bdr\Tasks\Factory;

use Jeancsil\Bdr\Tasks\Dto\TaskDto;
use Jeancsil\Bdr\Tasks\Task;

/**
 * @author Jean Carlos <jeancsil@gmail.com>
 */
class TaskDtoFactory
{
    /**
     * @param \stdClass $request
     * @return TaskDto
     */
    public function createFromRequest(\stdClass $request)
    {
        $dto = new TaskDto();
        $dto->id = property_exists($request, 'id') ? $request->id : null;
        $dto->priority = property_exists($request, 'priority') ? $request->priority : null;
        $dto->title = property_exists($request, 'title') ? $request->title : null;
        $dto->description= property_exists($request, 'description') ? $request->description : null;

        return $dto;
    }

    /**
     * @param Task $task
     * @return TaskDto
     */
    public function createFromEntity(Task $task)
    {
        $dto = new TaskDto();
        $dto->id = $task->getId();
        $dto->priority = $task->getPriority();
        $dto->title = $task->getTitle();
        $dto->description = $task->getDescription();
        $dto->created = $task->getCreatedAt()->format('d/m/Y H:i:s');
        $dto->updated= $task->getUpdatedAt()->format('d/m/Y H:i:s');

        return $dto;
    }
}
