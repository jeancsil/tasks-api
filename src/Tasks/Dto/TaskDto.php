<?php
namespace Jeancsil\Bdr\Tasks\Dto;

/**
 * @author Jean Carlos <jeancsil@gmail.com>
 */
class TaskDto
{
    /**
     * @var int
     */
    public $id;

    /**
     * @var string
     */
    public $title;

    /**
     * @var string
     */
    public $priority;

    /**
     * @var string
     */
    public $description;

    /**
     * @var \DateTime
     */
    public $created;

    /**
     * @var \DateTime
     */
    public $updated;
}
