<?php
namespace Jeancsil\Bdr\Tasks\Service;

use Jeancsil\Bdr\Tasks\Dto\TaskDto;
use Jeancsil\Bdr\Tasks\Task;

/**
 * @author Jean Carlos <jeancsil@gmail.com>
 */
interface TaskUpdaterInterface
{
    /**
     * @param TaskDto $taskDto
     * @return Task
     */
    public function update(TaskDto $taskDto);

    /**
     * @param $id
     */
    public function delete($id);
}
