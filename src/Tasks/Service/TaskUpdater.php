<?php
namespace Jeancsil\Bdr\Tasks\Service;

use Doctrine\ORM\EntityNotFoundException;
use Jeancsil\Bdr\Tasks\Dto\TaskDto;
use Jeancsil\Bdr\Tasks\Repository\TaskRepositoryInterface;
use Jeancsil\Bdr\Tasks\Task;

/**
 * @author Jean Carlos <jeancsil@gmail.com>
 */
class TaskUpdater implements TaskUpdaterInterface
{
    /**
     * @var TaskRepositoryInterface
     */
    private $repository;

    /**
     * @param TaskRepositoryInterface $repository
     */
    public function __construct(TaskRepositoryInterface $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @param TaskDto $taskDto
     * @return Task
     * @throws EntityNotFoundException
     */
    public function update(TaskDto $taskDto)
    {
        if (!$task = $this->repository->find($taskDto->id)) {
            throw new EntityNotFoundException(sprintf('The task with id %s was not found.', $taskDto->id));
        }

        if ($taskDto->title) {
            $task->setTitle($taskDto->title);
        }

        if ($taskDto->priority) {
            $task->setPriority($taskDto->priority);
        }

        if ($taskDto->description) {
            $task->setDescription($taskDto->description);
        }

        $task->setUpdatedAt(new \DateTime());

        $this->repository->persist($task);

        return $task;
    }

    /**
     * @param $taskId
     * @throws EntityNotFoundException
     */
    public function delete($taskId)
    {
        if (!$task = $this->repository->find($taskId)) {
            throw new EntityNotFoundException(sprintf('The task with id %s was not found.', $taskId));
        }

        $task->setDeletedAt(new \DateTime());

        $this->repository->persist($task);
    }
}
