<?php
namespace Jeancsil\Bdr\Tasks\Service;

use Jeancsil\Bdr\Tasks\Dto\TaskDto;
use Jeancsil\Bdr\Tasks\Repository\TaskRepositoryInterface;
use Jeancsil\Bdr\Tasks\Task;

/**
 * @author Jean Carlos <jeancsil@gmail.com>
 */
class TaskCreator implements TaskCreatorInterface
{
    /**
     * @var TaskRepositoryInterface
     */
    private $repository;

    /**
     * @param TaskRepositoryInterface $repository
     */
    public function __construct(TaskRepositoryInterface $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @param TaskDto $taskDto
     * @return Task
     */
    public function create(TaskDto $taskDto)
    {
        $task = new Task();
        $task->setTitle($taskDto->title);
        $task->setDescription($taskDto->description);
        $task->setPriority($taskDto->priority);

        $this->repository->persist($task);

        return $task;
    }
}
