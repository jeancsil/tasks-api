<?php
namespace Jeancsil\Bdr\Tasks\Service;

use Jeancsil\Bdr\Tasks\Dto\TaskDto;

/**
 * @author Jean Carlos <jeancsil@gmail.com>
 */
interface TaskCreatorInterface
{
    public function create(TaskDto $taskDto);
}
