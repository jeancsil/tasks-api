<?php
namespace Jeancsil\Bdr\Tasks;

/**
 * @author Jean Carlos <jeancsil@gmail.com>
 */
class Priority
{
    const TRIVIAL = 'trivial';
    const NORMAL = 'normal';
    const URGENT = 'urgent';
    const BLOCKER = 'blocker';
    const CRITICAL = 'critical';

    /**
     * @return array
     */
    public static function toArray()
    {
        return (new \ReflectionClass(__CLASS__))->getConstants();
    }
}
