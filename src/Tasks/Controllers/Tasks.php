<?php

namespace Jeancsil\Bdr\Tasks\Controllers;

use Jeancsil\Bdr\Entities\Validator\Errors\ValidationException;
use Jeancsil\Bdr\Routing\AbstractController;
use Jeancsil\Bdr\Tasks\Factory\TaskDtoFactory;
use Jeancsil\Bdr\Tasks\Repository\TaskRepositoryInterface;
use Jeancsil\Bdr\Tasks\Service\TaskCreatorInterface;
use Jeancsil\Bdr\Tasks\Service\TaskUpdaterInterface;
use Jeancsil\Bdr\Tasks\Validator\TaskRequestValidator;
use Lcobucci\ActionMapper2\Routing\Annotation\Inject;
use Lcobucci\ActionMapper2\Routing\Annotation\Route;

/**
 * @author Jean Carlos <jeancsil@gmail.conm>
 */
class Tasks extends AbstractController
{
    /**
     * @var TaskRepositoryInterface
     */
    private $repository;

    /**
     * @var TaskRequestValidator
     */
    private $requestValidator;

    /**
     * @var TaskDtoFactory
     */
    private $dtoFactory;

    /**
     * @var TaskCreatorInterface
     */
    private $taskCreator;

    /**
     * @var TaskUpdaterInterface
     */
    private $taskUpdater;

    /**
     * @Inject({
     *  "tasks.repository",
     *  "tasks.request.validator",
     *  "tasks.dto.factory",
     *  "tasks.creator",
     *  "tasks.updater"
     * })
     *
     * @param TaskRepositoryInterface $repository
     * @param TaskRequestValidator $requestValidator
     * @param TaskDtoFactory $dtoFactory
     * @param TaskCreatorInterface $taskCreator
     * @param TaskUpdaterInterface $taskUpdater
     */
    public function __construct(
        TaskRepositoryInterface $repository,
        TaskRequestValidator $requestValidator,
        TaskDtoFactory $dtoFactory,
        TaskCreatorInterface $taskCreator,
        TaskUpdaterInterface $taskUpdater
    ) {
        $this->repository = $repository;
        $this->requestValidator = $requestValidator;
        $this->dtoFactory = $dtoFactory;
        $this->taskCreator = $taskCreator;
        $this->taskUpdater = $taskUpdater;
    }

    /**
     * @Route("/", methods={"POST"})
     */
    public function create()
    {
        try {
            $requestBody = $this->getRequestBodyAsJson();

            $this->requestValidator->setRequest($requestBody);
            $this->requestValidator->validateCreation();

            $this->response->setStatusCode(201);

            return json_encode(
                $this->taskCreator->create(
                    $this->dtoFactory->createFromRequest($requestBody)
                )
            );
        } catch (\Exception $e) {
            $this->response->setStatusCode(400);
            return json_encode(['error' => $e->getMessage()]);
        }
    }

    /**
     * @Route("/", methods={"DELETE"})
     */
    public function delete()
    {
        try {
            $requestBody = $this->getRequestBodyAsJson();

            if (!property_exists($requestBody, 'id') || $requestBody->id <= 0) {
                throw new ValidationException('The id is mandatory and must be numeric.');
            }

            $this->response->setStatusCode(200);
            $this->taskUpdater->delete($requestBody->id);

            return json_encode(['success' => true]);
        } catch (\Exception $e) {
            $this->response->setStatusCode(400);
            return json_encode(['error' => $e->getMessage()]);
        }
    }

    /**
     * @Route("/", methods={"PUT"})
     */
    public function update()
    {
        try {
            $requestBody = $this->getRequestBodyAsJson();

            $this->requestValidator->setRequest($requestBody);
            $this->requestValidator->validateUpdate();

            $this->response->setStatusCode(200);

            return json_encode(
                $this->taskUpdater->update(
                    $this->dtoFactory->createFromRequest($requestBody)
                )
            );
        } catch (\Exception $e) {
            $this->response->setStatusCode(400);
            return json_encode(['error' => $e->getMessage()]);
        }
    }

    /**
     * @Route("/", methods={"PATCH"})
     */
    public function updatePartial()
    {
        try {
            $requestBody = $this->getRequestBodyAsJson();

            $this->requestValidator->setRequest($requestBody);
            $this->requestValidator->validatePartialUpdate();

            $this->response->setStatusCode(200);

            return json_encode(
                $this->taskUpdater->update(
                    $this->dtoFactory->createFromRequest($requestBody)
                )
            );
        } catch (\Exception $e) {
            $this->response->setStatusCode(400);
            return json_encode(['error' => $e->getMessage()]);
        }
    }

    /**
     * @Route("/", methods={"GET"})
     */
    public function listAll()
    {
        try {
            $tasks = $this->repository->findLimited(5);

            $tasksDto = [];
            foreach ($tasks as $task) {
                $tasksDto[] = $this->dtoFactory->createFromEntity($task);
            }

            return json_encode($tasksDto);
        } catch (\Exception $e) {
            $this->response->setStatusCode(500);
            return json_encode(['error' => $e->getMessage()]);
        }
    }
}
