<?php

namespace Jeancsil\Bdr\Tasks\Validator;

use Jeancsil\Bdr\Entities\Validator\AbstractValidator;
use Jeancsil\Bdr\Entities\Validator\Errors\ValidationException;
use Jeancsil\Bdr\Tasks\Priority;

/**
 * @author Jean Carlos <jeancsil@gmail.com>
 */
class TaskRequestValidator extends AbstractValidator
{
    /**
     * @inheritdoc
     */
    public function validateCreation()
    {
        if (!$this->propertyExists('priority') || !$this->isPriorityValid()) {
            throw new ValidationException(sprintf('The priority "%s" does not exists.', $this->request->priority));
        }

        if (!$this->propertyExists('title')) {
            throw new ValidationException('The task title is mandatory');
        }

        if (!$this->propertyExists('description')) {
            throw new ValidationException('The task description is mandatory');
        }
    }

    /**
     * @inheritdoc
     */
    public function validateUpdate()
    {
        if (!$this->isIdValid()) {
            throw new ValidationException('The task id (integer) is mandatory.');
        }

        $this->validateCreation();
    }

    public function validatePartialUpdate()
    {
        if (!$this->isIdValid()) {
            throw new ValidationException('The task id (integer) is mandatory.');
        }

        if ($this->propertyExists('priority') && !$this->isPriorityValid()) {
            throw new ValidationException(sprintf('The priority "%s" does not exists.', $this->request->priority));
        }

        if ($this->isTitleValid()) {
            throw new ValidationException(sprintf('The title "%s" is not valid.', $this->request->title));
        }

        if ($this->isDescriptionValid()) {
            throw new ValidationException(sprintf('The description "%s" is not valid.', $this->request->description));
        }
    }

    /**
     * @return bool
     */
    private function isPriorityValid()
    {
        return in_array($this->request->priority, Priority::toArray());
    }

    /**
     * @return bool
     */
    private function isIdValid()
    {
        return $this->propertyExists('id') && is_numeric($this->request->id) && $this->request->id > 0;
    }

    /**
     * @return bool
     */
    private function isTitleValid()
    {
        return $this->propertyExists('title') && strlen($this->request->title) <= 120;
    }

    /**
     * @return bool
     */
    private function isDescriptionValid()
    {
        return $this->propertyExists('description') && strlen($this->request->description) > 1000;
    }
}
