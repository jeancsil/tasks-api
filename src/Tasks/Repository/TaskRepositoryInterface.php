<?php
namespace Jeancsil\Bdr\Tasks\Repository;

use Jeancsil\Bdr\Tasks\Task;

/**
 * @author Jean Carlos <jeancsil@gmail.com>
 */
interface TaskRepositoryInterface
{
    /**
     * @param int $size
     * @return Task[]
     */
    public function findLimited($size = 10);

    /**
     * @param int $id
     *
     * @return Task
     */
    public function find($id);

    /**
     * @return Task[]
     */
    public function findAll();

    /**
     * Persist the Task object on database.
     * @param Task $task
     */
    public function persist(Task $task);
}
