<?php
namespace Jeancsil\Bdr\Tasks\Repository;

use Doctrine\ORM\EntityRepository;
use Jeancsil\Bdr\Tasks\Task;

/**
 * @author Jean Carlos <jeancsil@gmail.com>
 */
class TaskRepository extends EntityRepository implements TaskRepositoryInterface
{
    /**
     * @param int $size
     * @return array
     */
    public function findLimited($size = 10)
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.deletedAt is null')
            ->orderBy('t.createdAt', 'desc')
            ->setMaxResults($size)
            ->getQuery()
            ->getResult();
    }

    /**
     * {@inheritdoc}
     */
    public function persist(Task $task)
    {
        $entityManager = $this->getEntityManager();

        $entityManager->persist($task);
        $entityManager->flush($task);
    }
}
