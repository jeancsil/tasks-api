<?php
namespace Jeancsil\Bdr\Tasks;

use Doctrine\ORM\Mapping as ORM;
use JsonSerializable;
use Jeancsil\Bdr\Entities\PersistentIdentifierTrait;
use Jeancsil\Bdr\Entities\PersistentTimeTrait;

/**
 * @ORM\Entity(repositoryClass="Jeancsil\Bdr\Tasks\Repository\TaskRepository")
 * @ORM\Table("Task")
 *
 * @author Jean Carlos <jeancsil@gmail.com
 */
class Task implements JsonSerializable
{
    use PersistentIdentifierTrait;
    use PersistentTimeTrait;

    /**
     * @ORM\Column(type="string")
     *
     * @var string
     */
    private $priority;

    /**
     * @ORM\Column(type="string")
     *
     * @var string
     */
    private $title;

    /**
     * @ORM\Column(type="text")
     *
     * @var string
     */
    private $description;

    public function __construct()
    {
        $this->createdAt = new \DateTime();
        $this->updatedAt= new \DateTime();
    }

    /**
     * @return mixed
     */
    public function getPriority()
    {
        return $this->priority;
    }

    /**
     * @param mixed $priority
     */
    public function setPriority($priority)
    {
        if (!in_array($priority, Priority::toArray())) {
            throw new \InvalidArgumentException('Wrong priority.');
        }

        $this->priority = $priority;
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param string $title
     */
    public function setTitle($title)
    {
        if (strlen($title) > 120) {
            throw new \InvalidArgumentException('The title max length is 120.');
        }

        $this->title = $title;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param string $description
     */
    public function setDescription($description)
    {
        if (strlen($description) > 1000) {
            throw new \InvalidArgumentException('The description max length is 1000.');
        }

        $this->description = $description;
    }

    /**
     * {@inheritdoc}
     */
    public function jsonSerialize()
    {
        return [
            'id' => $this->id,
            'priority' => $this->priority,
            'title' => $this->title,
            'description' => $this->description,
            'createdAt' => $this->createdAt->format('d/m/Y H:i:s'),
            'updatedAt' => $this->updatedAt->format('d/m/Y H:i:s')
        ];
    }
}
