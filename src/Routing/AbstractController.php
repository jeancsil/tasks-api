<?php

namespace Jeancsil\Bdr\Routing;

use Jeancsil\Bdr\Http\Errors\JsonParseException;
use Lcobucci\ActionMapper2\Routing\Controller;

/**
 * @author Jean Carlos <jeancsil@gmail.com>
 */
abstract class AbstractController extends Controller
{
    /**
     * @return \stdClass
     * @throws JsonParseException
     */
    protected function getRequestBodyAsJson()
    {
        $body = json_decode($this->request->getContent());

        if (json_last_error() != JSON_ERROR_NONE) {
            throw new JsonParseException(
                'The received JSON is invalid.',
                json_last_error()
            );
        }

        return $body;
    }
}
