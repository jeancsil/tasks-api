<?php

namespace Jeancsil\Bdr\Http\Errors;

/**
 * @author Jean Carlos <jeancsil@gmail.com>
 */
class JsonParseException extends \Exception
{
    
}
