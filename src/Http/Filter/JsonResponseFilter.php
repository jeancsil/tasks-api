<?php

namespace Jeancsil\Bdr\Http\Filter;

use Lcobucci\ActionMapper2\Routing\Filter;

/**
 * @author Jean Carlos <jeancsil@gmail.com>
 */
class JsonResponseFilter extends Filter
{
    /**
     * @inheritdoc
     */
    public function process()
    {
        $this->response->setContentType('application/json');
    }
}
