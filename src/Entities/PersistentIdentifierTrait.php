<?php

namespace Jeancsil\Bdr\Entities;

/**
 * @author Jean Carlos <jeancsil@gmail.com>
 */
trait PersistentIdentifierTrait
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     *
     * @var int
     */
    protected $id;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param integer $id
     * @throws \InvalidArgumentException
     */
    public function setId($id)
    {
        if (!is_numeric($id)) {
            throw new \InvalidArgumentException(sprintf('The id must be numeric. %s given', $id));
        }

        $this->id = $id;
    }
}
