<?php

namespace Jeancsil\Bdr\Entities\Validator;

/**
 * @author Jean Carlos <jeancsil@gmail.com>
 */
abstract class AbstractValidator implements ValidatorInterface
{
    /**
     * @var \stdClass
     */
    protected $request;

    /**
     * @param \stdClass $request
     */
    public function setRequest(\stdClass $request)
    {
        $this->request = $request;
    }

    /**
     * @param $property
     * @return bool
     */
    protected function propertyExists($property)
    {
        if (is_null($this->request)) {
            throw new \LogicException('The setRequest method must be called before.');
        }

        return property_exists($this->request, $property) === true;
    }
}
