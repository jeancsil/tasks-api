<?php

namespace Jeancsil\Bdr\Entities\Validator\Errors;

/**
 * @author Jean Carlos <jeancsil@gmail.com>
 */
class ValidationException extends \Exception
{
    
}
