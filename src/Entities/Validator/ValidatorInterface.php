<?php

namespace Jeancsil\Bdr\Entities\Validator;

use Jeancsil\Bdr\Entities\Validator\Errors\ValidationException;

/**
 * @author Jean Carlos <jeancsil@gmail.com>
 */
interface ValidatorInterface
{
    /**
     * @throws ValidationException
     */
    public function validateCreation();

    /**
     * @throws ValidationException
     */
    public function validateUpdate();

    /**
     * @throws ValidationException
     */
    public function validatePartialUpdate();
}
