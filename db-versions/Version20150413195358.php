<?php
namespace DoctrineMigrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;
use Jeancsil\Bdr\Tasks\Priority;

class Version20150413195358 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql("
          INSERT INTO Task (priority, title, description, createdAt, updatedAt)
          VALUES ('".Priority::BLOCKER."', 'Criar testes de unidade', 'Utilizando boas práticas.', NOW(), NOW());

          INSERT INTO Task (priority, title, description, createdAt, updatedAt)
          VALUES ('".Priority::TRIVIAL."', 'Executar testes de unidade', 'Usando PHPUnit..', NOW(), NOW());
        ");
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('TRUNCATE Task;');
    }
}
