<?php
namespace Jeancsil\Bdr\Tasks;

/**
 * @author Jean Carlos <jeancsil@gmail.com>
 */
class TaskTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @test
     */
    public function testSimpleObjectCreationNotFails()
    {
        $task = new Task();
        $task->setTitle('This is the task title.');
        $task->setDescription('Just to remember that this task title does not have more than 120 chars!');
        $task->setPriority(Priority::NORMAL);

        $this->assertNotNull($task->getTitle());
        $this->assertNotNull($task->getDescription());
        $this->assertEquals($task->getPriority(), Priority::NORMAL);
    }

    /**
     * @test
     * @expectedException \InvalidArgumentException
     */
    public function testCreatingTaskTitleWithMoreThan120Characters()
    {
        $task = new Task();
        $task->setTitle(
            'This is a very
            BiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiG title.'
        );
    }

    /**
     * @test
     */
    public function testCreatingADummyTaskWillStartDateTimeObjects()
    {
        $task = new Task();
        $this->assertTrue($task->getCreatedAt() instanceof \DateTime);
        $this->assertTrue($task->getUpdatedAt() instanceof \DateTime);
    }


    /**
     * @test
     */
    public function testCreatingADummyTaskWillNotStartTheDeletedAtProperty()
    {
        $task = new Task();
        $this->assertNull($task->getDeletedAt());
    }

    /**
     * @test
     */
    public function testIfIsDeletedMethodWillWorkAsExpected()
    {
        $task = new Task();
        $this->assertFalse($task->isDeleted());

        $task->setDeletedAt(new \DateTime());
        $this->assertTrue($task->isDeleted());

        $task->setDeletedAt(null);
        $this->assertFalse($task->isDeleted());
    }

    /**
     * @test
     * @expectedException \InvalidArgumentException
     */
    public function testTaskWithWrongPriorities()
    {
        $task = new Task();
        $task->setPriority('wrong_priority');
    }
}
