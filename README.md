# Tiny tasks API 
##### Jean Carlos Silva [<jeancsil@gmail.com>](jeancsil@gmail.com)

- Sumário:
  - Requerimentos
  - Instalação
  - Exemplos

###Requerimentos
> Criar um vhost que aponte para o arquivo your-project-path/public/index.php e seja acessível como no exemplo:
http://api.tasks.com.br/

> Adicionar a linha ao /etc/hosts:
```sh
127.0.0.1 www.api.tasks.com.br api.tasks.com.br
```

> Composer: [https://getcomposer.org/download/](https://getcomposer.org/download/)

> Versão compatível com PHP versão 5.4+ por usar [Traits](http://php.net/traits).

### Instalação
Alterar os parâmetros de conexão com o banco de dados em: [tasks-api]/config/di-container/environment.xml
```xml
<parameter key="db.host">127.0.0.1</parameter>
<parameter key="db.schema">bdr_db</parameter>
<parameter key="db.user">bdr_user</parameter>
<parameter key="db.passwd">bdr_pass</parameter>
```
Dar permissão de escrita para todos no diretório temporário.
```sh
$ sudo chmod 777 your-project-path/tmp -R
```
Criar o banco de dados "bdr_db" (ou outro nome que tenha sido dado).
```sql
create database if not exists bdr_db;
```
```sh
$ composer install
```
Executar migrations do [Doctrine](http://docs.doctrine-project.org/projects/doctrine-migrations/en/latest/).
```sh
cd your-project-path/ && ./vendor/bin/doctrine migrations:migrate --no-interaction
```
###Exemplos
Todos os requests devem ser feitos utilizando o header *x-www-form-urlencoded* e o conteúdo do corpo deve ser no formato *JSON*.

Recuperar tarefas:
```http
GET /tasks HTTP/1.1
```
Criar uma tarefa:
```http
POST /tasks HTTP/1.1
{
  "priority": "blocker",
  "title": "Título da tarefa",
  "description": "Descrição da tarefa"
}
```
Atualizar uma tarefa.
```http
PUT /tasks HTTP/1.1
{
  "id":  1,
  "priority": "normal",
  "title": "Novo título para a tarefa.",
  "description": "Nova descrição para a tarefa"
}
```
Alterar parcialmente uma tarefa.
```http
PATCH /tasks HTTP/1.1
{
  "id":  1,
  "priority": "critical"
}
```
Remover uma tarefa.
```http
DELETE /tasks HTTP/1.1
{
  "id":  1
}
```